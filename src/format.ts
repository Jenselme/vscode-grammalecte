import * as vscode from "vscode";
import { execFormatter, isWindows, readGrammalecteOutput } from "./utils";

export const formatFileWithGrammalecte = (editor: vscode.TextEditor) =>
    execFormatter(editor.document.uri.fsPath)
        .then(output => readGrammalecteOutput(editor.document.uri.fsPath, output.stdout))
        .then(response => {
            // We must remove the two first lines that are about Python and Grammalecte versions.
            // On Windows, in the output file, these lines are not printed, so we don't have to remove them.
            const formattedText = isWindows() ? response : response.split("\n").splice(2).join("\n");
            const initialText = editor.document.getText();
            const formattedTextWithRestoredIndentation = restoreIndent(initialText, formattedText);

            editor.edit(editBuilder => {
                /*intentionally missing the '-1' */
                const invalidRange = new vscode.Range(0, 0, editor.document.lineCount, 0);
                const fullRange = editor.document.validateRange(invalidRange);
                editBuilder.replace(fullRange, formattedTextWithRestoredIndentation);
            });
        }).catch(error => {
            vscode.window.showInformationMessage(`Formating file failed with message: ${error}`);
        });

const restoreIndent = (initialText: string, formattedText: string): string => {
    if (!vscode.workspace.getConfiguration("grammalecte").restoreInitialWhitespacesAfterFormat) {
        return formattedText;
    }

    const linesOfInitialText = initialText.split("\n");
    const linesOfFormattedText = formattedText.split("\n");
    for (const [index, line] of linesOfInitialText.entries()) {
        const nbIndent = /^ +/.exec(line);
        if (nbIndent !== null) {
            linesOfFormattedText[index] = nbIndent[0] + linesOfFormattedText[index];
        }
    }
    return linesOfFormattedText.join("\n");
};
