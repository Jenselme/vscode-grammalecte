import * as vscode from "vscode";
import { execGrammalecte, writeFilePromise, debounce, readGrammalecteOutput } from "./utils";
import { file } from "tmp-promise";

type GrammalecteResponse = {
    data: ParagraphError[];
};

type ParagraphError = {
    iParagraph: number;
    lGrammarErrors: GrammarError[];
    lSpellingErrors: SpellingError[];
};

type GrammarError = {
    aSuggestions: string[];
    nStart: number;
    nEnd: number;
    sLineId: string;
    sMessage: string;
    sRuleId: string;
    sType: string;
};

type SpellingError = {
    aSuggestions: string[];
    i: number;
    nStart: number;
    nEnd: number;
    sType: string;
    sValue: string;
};

type ErrorForReporting = {
    line: number;
    startPos: number;
    endPos: number;
    message: string;
    suggestion: string[];
};

export const runGrammalecte = (documentPath: string, documentUri: vscode.Uri, errors: vscode.DiagnosticCollection) => {
    return execGrammalecte(documentPath).then(response => {
        return readGrammalecteOutput(documentPath, response.stdout);
    }).then((output) => {
        const grammalecteResponse: GrammalecteResponse = JSON.parse(output);
        const reportedErrors = grammalecteResponse.data
            .map(createErrorForReporting)
            .reduce((acc, errorsForReporting) => ([...acc, ...errorsForReporting]), [])
            .map(createError)
            .filter(error => {
                if (vscode.workspace
                    .getConfiguration("grammalecte")
                    .reportSpacesOnLineStart) {
                    return true;
                }

                return !error.message.startsWith("Espace·s en début de ligne à supprimer");
            });
        errors.set(documentUri, reportedErrors);
    }).catch(error => {
        vscode.window.showInformationMessage(`Running grammalecte failed with message: ${error}`);
    });
};

const createErrorForReporting = (paragraphError: ParagraphError): ErrorForReporting[] => {
    return [
        ...paragraphError.lGrammarErrors.map(grammarError => ({
            line: paragraphError.iParagraph,
            startPos: grammarError.nStart,
            endPos: grammarError.nEnd,
            message: grammarError.sMessage,
            suggestion: grammarError.aSuggestions,
        })),
        ...paragraphError.lSpellingErrors.map(spellingError => ({
            line: paragraphError.iParagraph,
            startPos: spellingError.nStart,
            endPos: spellingError.nEnd,
            message: "",
            suggestion: spellingError.aSuggestions,
        })),
    ];
};

const createError = (error: ErrorForReporting) => ({
    message: formatErrorMessage(error),
    range: new vscode.Range(
        new vscode.Position(error.line - 1, error.startPos),
        new vscode.Position(error.line - 1, error.endPos)
    ),
    severity: vscode.DiagnosticSeverity.Information,
});

const formatErrorMessage = (error: ErrorForReporting): string => `${error.message} ${error.suggestion}`.trim();

const runGrammalecteOnTextChanged = (document: vscode.TextDocument, errors: vscode.DiagnosticCollection) => {
    file()
        .then(({ path, cleanup }) => {
            return writeFilePromise(path, document.getText())
                .then(() => runGrammalecte(path, document.uri, errors))
                .then(() => cleanup());
        });
};

export const runGrammalecteOnTextChangedDebounced = debounce(runGrammalecteOnTextChanged, 1_000);
