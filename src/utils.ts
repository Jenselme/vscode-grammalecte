import * as vscode from "vscode";
import { exec } from "child_process";
import { promisify } from "util";
import * as fs from "fs";
import { extname, parse, join as joinPath } from "path";
const untildify = require("untildify");

const execPromise = promisify(exec);
export const writeFilePromise = promisify(fs.writeFile);
export const readFilePromise = promisify(fs.readFile);

/**
 * Join shell args in a safe manner that will avoid some shell attacks.
 * We do this by quoting each argument before create the command string.
 */
const joinArgs = (args: string[]) => args
    .map((arg) => {
        // On Windows, escaping prevents the command and options to be used correctly.
        if (isWindows()) {
            return arg;
        }

        return `"${arg}"`;
    })
    .join(" ");

const espacePath = (path: string): string => path.replace(/"/g, "\\\"").replace(/\$/g, "\\\$");

export const execFormatter = (path: string) =>
    execPromise(joinArgs([
        untildify(vscode.workspace.getConfiguration("grammalecte").pythonPath),
        untildify(vscode.workspace.getConfiguration("grammalecte").pathToGrammalecteCli),
        "--textformatteronly",
        "--file",
        espacePath(path),
    ]));

export const execGrammalecte = (path: string) => {
    const extraOptions = getExtraGrammalecteOptions();

    return execPromise(joinArgs([
        untildify(vscode.workspace.getConfiguration("grammalecte").pythonPath),
        untildify(vscode.workspace.getConfiguration("grammalecte").pathToGrammalecteCli),
        "--only_when_errors",
        "--with_spell_sugg",
        "--json",
        ...extraOptions,
        "--file",
        espacePath(path),
    ]));
};

const getExtraGrammalecteOptions = (): string[] => {
    const extraOptions: string[] = [];
    if (!vscode.workspace.getConfiguration("grammalecte").reportFormatError) {
        extraOptions.push(
            "typo",
            "unit",
            "apos",
            "nbsp",
        );
    }
    if (!vscode.workspace.getConfiguration("grammalecte").reportComma) {
        extraOptions.push(
            "virg",
        );
    }

    return extraOptions.length > 0 ? ["--opt_off", ...extraOptions] : [];
};

export const debounce = (func: Function, waitTime: number) => {
    let timeout: ReturnType<typeof setTimeout> | null = null;

    return (...args: unknown[]) => {
        if (timeout !== null) {
            clearTimeout(timeout);
        }

        timeout = setTimeout(() => func(...args), waitTime);
    };
};

export const isFileExtensionAllowed = (fileName: string): boolean => {
    const allowedExtension: string[] = vscode.workspace
        .getConfiguration("grammalecte")
        .allowedExtension.split(",")
        .map((ext: string) => ext.trim());
    return allowedExtension.includes(extname(fileName));
};

export const checkSetup = () => checkPythonWorks().then(() => checkGrammalecteWorks());

const checkPythonWorks = () =>
    execPromise(joinArgs([
        untildify(vscode.workspace.getConfiguration("grammalecte").pythonPath),
        "--version",
    ])).then(null, (response) => {
        if (!/^Python 3\\./.test(response.stdout)) {
            return Promise.reject("Failed to find Python");
        }

        return true;
    });

const checkGrammalecteWorks = () =>
    execPromise(joinArgs([
        untildify(vscode.workspace.getConfiguration("grammalecte").pythonPath),
        untildify(vscode.workspace.getConfiguration("grammalecte").pathToGrammalecteCli),
        "--help",
    ])).then(null, (response) => {
        if (!/^usage: grammalecte-cli\\.py/.test(response.stdout)) {
            return Promise.reject("Failed to find Grammalecte");
        }

        return true;
    });

export const isWindows = () => process.platform === "win32";

export const readGrammalecteOutput = (documentPath: string, responseOutput: string): Promise<string> => {
    if (isWindows()) {
        const {dir, name, ext} = parse(documentPath);
        // Temp file are without an extension and Grammalecte will save the file without
        // the last char in the name.
        const resultFileName = !!ext ? name : name.substring(0, name.length - 1);
        return readFilePromise(joinPath(dir, `${resultFileName}.res.txt`))
            .then(value => value.toString());
    }

    return Promise.resolve(responseOutput);
};
