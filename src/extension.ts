// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";
import { formatFileWithGrammalecte } from "./format";
import { runGrammalecte, runGrammalecteOnTextChangedDebounced } from "./grammalecte";
import { checkSetup, isFileExtensionAllowed } from "./utils";
import { highlightSpecialChar } from "./highlight-special-chars";

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
    checkThenRun(context);

    context.subscriptions.push(vscode.workspace.onDidChangeConfiguration(() => {
        checkThenRun(context);
    }));
}

const checkThenRun = (context: vscode.ExtensionContext) => {
    checkSetup().then(
        () => startGrammalecte(context),
        (error) => vscode.window.showInformationMessage(`Grammalecte failed to initialize: ${error}`),
    );
};

const startGrammalecte = (context: vscode.ExtensionContext) => {
    for (const subscription of context.subscriptions) {
        subscription.dispose();
    }

    const allowedExtension: string = vscode.workspace
        .getConfiguration("grammalecte")
        .allowedExtension;

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
    const formatCommand = vscode.commands.registerCommand("grammalecte.format", () => {
        const editor = vscode.window.activeTextEditor;
        if (editor && checkCommandCanRun(allowedExtension)) {
            editor.document.save().then(() => formatFileWithGrammalecte(editor));
        }
    });
    context.subscriptions.push(formatCommand);

    const diagnostics = vscode.languages.createDiagnosticCollection("grammalecte");
    if (
        vscode.window.activeTextEditor &&
        checkCanRunOnTheFly(vscode.window.activeTextEditor.document.fileName)
    ) {
        runGrammalecte(
            vscode.window.activeTextEditor.document.uri.fsPath,
            vscode.window.activeTextEditor.document.uri,
            diagnostics,
        );
        highlightSpecialChar(vscode.window.activeTextEditor);
    }

    context.subscriptions.push(vscode.window.onDidChangeActiveTextEditor(editor => {
        if (editor && checkCanRunOnTheFly(editor.document.fileName)) {
            runGrammalecte(
                editor.document.uri.fsPath,
                editor.document.uri,
                diagnostics,
            );
            highlightSpecialChar(editor);
        }
    }));

    context.subscriptions.push(vscode.workspace.onDidChangeTextDocument(event => {
        if (
            event.document.isDirty &&
            event.contentChanges.length > 0 &&
            checkCanRunOnTheFly(event.document.fileName)
        ) {
            runGrammalecteOnTextChangedDebounced(event.document, diagnostics);
            highlightSpecialChar(vscode.window.activeTextEditor);
        }
    }));

    context.subscriptions.push(vscode.workspace.onDidSaveTextDocument((document) => {
        if (checkCanRunOnTheFly(document.fileName)) {
            runGrammalecteOnTextChangedDebounced(document, diagnostics);
            highlightSpecialChar(vscode.window.activeTextEditor);
        }
    }));

    context.subscriptions.push(vscode.workspace.onDidCloseTextDocument((document) => {
        if (checkCanRunOnTheFly(document.fileName)) {
            diagnostics.delete(document.uri);
        }
    }));

    const checkWithGrammalecteCommand = vscode.commands.registerCommand("grammalecte.check", () => {
        const editor = vscode.window.activeTextEditor;
        if (editor && checkCommandCanRun(allowedExtension)) {
            runGrammalecte(
                editor.document.uri.fsPath,
                editor.document.uri,
                diagnostics,
            );
        }
    });
    context.subscriptions.push(checkWithGrammalecteCommand);

    const clearErrorsCommand = vscode.commands.registerCommand("grammalecte.clear", () => {
        diagnostics.clear();
    });
    context.subscriptions.push(clearErrorsCommand);
};

const checkCanRunOnTheFly = (fileName: string): boolean => {
    const runOnTheFly: boolean = vscode.workspace
        .getConfiguration("grammalecte")
        .runOnTheFly;

    return runOnTheFly && isFileExtensionAllowed(fileName);
};

const checkCommandCanRun = (allowedExtension: string): boolean => {
    const editor = vscode.window.activeTextEditor;
    if (editor === undefined) {
        vscode.window.showInformationMessage("This must be run against a file.");
        return false;
    }
    if (!isFileExtensionAllowed(editor.document.fileName)) {
        vscode.window.showInformationMessage(`This must be run against a ${allowedExtension} file.`);
        return false;
    }

    return true;
};

// this method is called when your extension is deactivated
export function deactivate() {}
