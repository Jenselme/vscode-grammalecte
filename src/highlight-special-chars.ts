import * as vscode from "vscode";

const UNBREAKABLE_SPACES = [
    String.fromCodePoint(0x00A0), // NO-BREAK SPACE
    String.fromCodePoint(0x202F), // NARROW NO-BREAK SPACE
    String.fromCodePoint(0x2007), // FIGURE SPACE
    String.fromCodePoint(0x2060), // WORD JOINER
];
const whitespaceDecorationType = vscode.window.createTextEditorDecorationType({
    borderColor: "grey",
    borderStyle: "solid",
    borderWidth: "1px",
    opacity: "0.5",
    dark: {
        borderColor: "white",
    },
});

export const highlightSpecialChar = (editor: vscode.TextEditor | undefined) => {
    if (!editor) {
        return;
    }

    const decorations = [];
    const unbreakableSpacesFinder = new RegExp("[" + UNBREAKABLE_SPACES.join("") + "]+", "g");

    let match: RegExpExecArray | null = null;
    do {
        match = unbreakableSpacesFinder.exec(editor.document.getText());
        if (match === null) {
            break;
        }

        const startPos = editor.document.positionAt(match.index);
        const endPos = editor.document.positionAt(match.index + match[0].length);
        const decoration = { range: new vscode.Range(startPos, endPos), hoverMessage: "Non breakable space" };
        decorations.push(decoration);
    } while (match);

    editor.setDecorations(whitespaceDecorationType, decorations);
};
