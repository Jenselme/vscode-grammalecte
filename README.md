# Grammalecte wrapper for Visual Studio Code/Wrapper pour Grammalecte

French version below. Version française plus bas.

## English version

This is a wrapper for the [Grammalecte](https://grammalecte.net/) French spell and grammar checker.
It will only work on French text as it doesn't support any other languages.
Just like Grammalecte itself, it runs entirely on your machine and doesn't send anything to a server.

### Requirements

To make it run, you will need:
* [Python 3](https://www.python.org/downloads/).
  * If you are on Linux, it is probably already installed and the default settings will work just fine. If it is not the case, install it with the standard way to install packages on your distribution.
  * If you are on Windows:
    * I suggest you install it from the [Microsoft Store](https://docs.python.org/3/using/windows.html#the-microsoft-store-package) if don't already have Python.
    * You will have to change the value of `grammalecte.pythonPath` to use `python.exe` instead of the default.
* [Grammalecte](https://grammalecte.net/#download): You must download it, un-compress it and change the value of `grammalecte.pathToGrammalecteCli` so that it points to the `grammalecte-cli.py` script.
  * If you are on Linux, something like the default value of `grammalecte.pathToGrammalecteCli` will work.
  * If you are on Windows, something like this will work `C:\Users\Bob\Downloads\grammalecte-v2.1.1\grammalecte-cli.py`.

### Installation

You can install the extension directly from [the store](https://marketplace.visualstudio.com/items?itemName=jenselme.grammalecte) or manually (to do this, you need [nodejs](https://nodejs.org/)):

1. Get the source code by:
   * downloading it [from the repo](https://gitlab.com/Jenselme/vscode-grammalecte) by clicking the download button.
   * cloning the code with `git clone https://gitlab.com/Jenselme/vscode-grammalecte.git`.
2. Extract the code and place yourself into the new folder.
3. Install all dependencies with `npm install`
4. Package the extension with `npm run package`
5. Install the generated `.vsix` file (it will be in the `vscode-grammalecte` folder): open the extension toolbar, click on the three dots in its upper left corner and select *Install from VSIX*.

### Features

* Spell and grammar checking. Checking is done on the fly as you type, open a new document or save. The errors are marked in the text and reported in the bottom panel of VSCode. You will also find there the suggestion made by Grammalecte.
* A command named `grammalecte.format` to format your files with Grammalecte so they respect French typography standard. For instance, it will replace base quotes `'` with a typographic version `’`, add unbreakable spaces before French quotes (`«` and `»`), question mark, exclamation mark…
* A command named `grammalecte.check` to manually check a file. This is meant to work with the `grammalecte.runOnTheFly` option.
* A command named `grammalecte.clear` to force a clearing of all errors in all documents.
* Unbreakable spaces are highlighted so you can spot them more easily.

### Settings

| Name | Description | Default Value | Recommandation |
|:-----|:------------|:--------------|:---------------|
| `grammalecte.allowedExtension` | Comma separated list of file extensions on which to enable the extension. | `.md,.rst,.adoc,.asciidoc,.creole` |
| `grammalecte.pythonPath` | Path to the Python 3 implementation to run Grammalecte. | `python3` | If Python 3 is in your PATH, you don't need to use an absolute path. |
| `grammalecte.pathToGrammalecteCli` | Absolute path to the Grammalecte CLI script | `~/.local/grammalecte/grammalecte-cli.py` | Unless you unpack it at the default location, it must be changed. |
| `grammalecte.reportSpacesOnLineStart` | Whether to report starting white spaces as errors. | `false` | If you work on Markdown files with indented lines, it's better to keep this at `false`. |
| `grammalecte.restoreInitialWhitespacesAfterFormat` | Whether to restore initial indentation on line after formatting a file. | `true` | If you work on Markdown files with indented lines, it's better to keep this at `true` otherwise formatted document will be messed up. |
| `grammalecte.reportFormatError` | Whether to report formatting error that would be fixed by the format command. | `true` | |
| `grammalecte.reportComma` | Whether to report missing comas. | `true` | |
| `grammalecte.runOnTheFly` | Whether to run Grammalecte on the fly, as you type or save. | `true` | If you find this confusing and want to see and handle errors only when you choose, disable this and use the `grammalecte.check` and `grammalecte.clear` commands. |

### Licensing

* This plugin is not affiliated with Grammalecte.
* This plugin is licensed under the MIT license, the text is present in this repository.
* Grammalecte is publish under the GPLv3 license.
  * The fact that the programs have different licenses isn't be a problem: this VSCode extension only make basic calls to the Grammalecte CLI. This case is covered [here](https://www.gnu.org/licenses/gpl-faq.en.html#GPLPlugins) if you want to learn more about this.

## Version française

Cette extension est un wrapper pour le correcteur orthographique et grammatical [Grammalecte](https://grammalecte.net/).
Elle ne peut fonctionner que sur des textes français, seule langue supportée par Grammalecte.
Comme Grammalecte, tout est exécuté sur votre machine, rien n'est envoyés sur des serveurs distants.

### Prérequis

Pour faire fonctionner cette extension, il faut:
* [Python 3](https://www.python.org/downloads/).
  * Si vous utilisez Linux, Python 3 est probablement déjà installé et la configuration par défaut trouvera cette installation. Si ce n'est pas le cas, utilisez la méthode standard d'installation de votre distribution.
  * Si vous utilisez Windows:
    * Je vous conseille d'installer Python via le [Microsoft Store](https://docs.python.org/fr/3/using/windows.html#the-microsoft-store-package) si vous ne l'avez pas déjà.
    * Vous devez changer la valeur de `grammalecte.pythonPath` pour utiliser `python.exe`.
* [Grammalecte](https://grammalecte.net/#download): vous devez le télécharger, le décompresser et changer la valeur de `grammalecte.pathToGrammalecteCli` pour y mettre le chemin vers le script `grammalecte-cli.py`.
  * Sous Linux, vous pouvez vous inspirer de la valeur par défaut.
  * Sous Windows, il faut utiliser quelque chose comme ça: `C:\Users\Bob\Downloads\grammalecte-v2.1.1\grammalecte-cli.py`

### Installation

Vous pouvez installer l’extension directement depuis [le site de VSCode](https://marketplace.visualstudio.com/items?itemName=jenselme.grammalecte) ou manuellement (pour cela, vous aurez besoin de [nodejs](https://nodejs.org/)):

1. Récupérez le code source:
   * en le téléchargeant [depuis le dépôt](https://gitlab.com/Jenselme/vscode-grammalecte) en cliquant sur le bouton de téléchargement
   * en le clonant avec `git clone https://gitlab.com/Jenselme/vscode-grammalecte.git`
2. Extrayez le code source et allez dans le dossier où vous avez extrait le code.
3. Installez les dépendances avec `npm install`
4. Créez le fichier à installer avec `npm run package`
5. Installez le fichier `.vsix` ainsi généré (il sera dans le dossier `vscode-grammalecte`): ouvrez le panneau des extensions, cliquer sur les trois points dans son coin supérieur droit et sélectionnez *Installer à partir de VSIX*.

### Fonctionnalités

* Correction orthographique et grammaticale. La correction est lancée au fur et à mesure de la frappe, quand vous ouvrez un nouveau document ou sauvegardez. Les erreurs sont marquées sous le texte et listées dans le panneau inférieur de VSCode. Vous y verrez aussi les suggestions de Grammalecte.
* Une commande nommée `grammalecte.format` qui va formatter le fichier courant suivant les règles de typographie française. Par exemple, elle va remplacer les apostrophes simples par des apostrophes typographiques, ajouter une espace insécable avant les guillemets, les points d'exclamations…
* Une commande nommée `grammalecte.check` pour lancer les vérifications sur un fichier manuellement. Cette commande va de pair avec l'option `grammalecte.runOnTheFly` (voir plus bas).
* Une commande nommée `grammalecte.clear` pour forcer la suppression de toutes les erreurs dans tous les documents. Cette commande va de pair avec la commande précédente.
* Les espaces insécables sont mises en avant pour faciliter leur identification.

### Paramètres

| Name | Description | Default Value | Recommandation |
|:-----|:------------|:--------------|:---------------|
| `grammalecte.allowedExtension` | Liste d'extensions de fichier chacune séparées par des virgules pour lesquelles l'extension sera active. | `.md,.rst,.adoc,.asciidoc,.creole` |
| `grammalecte.pythonPath` | Chemin vers la version de Python 3 à utiliser pour lancer Grammalecte. | `python3` | Si Python 3 est dans votre PATH, vous n'avez pas besoin d'utiliser un chemin absolu. |
| `grammalecte.pathToGrammalecteCli` | Chemin absolu vers la CLI de Grammalecte. | `~/.local/grammalecte/grammalecte-cli.py` | Il faut changer cette valeur à moins d'utiliser l'emplacement par défaut et d'être sous Linux |
| `grammalecte.reportSpacesOnLineStart` | Rapporter les espaces en début de ligne comme une erreur. | `false` | Si vous travailler avec des fichiers Markdown qui ont fréquemment des espaces attendus en début de ligne, mieux vaut laisser cette option à faux. |
| `grammalecte.restoreInitialWhitespacesAfterFormat` | Conserver les indentations initiales de chaque ligne après un formatage du fichier. | `true` | Si vous travaillez avec des fichiers Markdown qui ont fréquemment des espaces attendus en début de ligne, mieux vaut laisser cette option à vrai sous peine de perdre l'indentation du document après un formatage. |
| `grammalecte.reportFormatError` | Montrer les erreurs de formatage qui seraient corrigées par la commande de formatage. | `true` | |
| `grammalecte.reportComma` | Montrer les soucis avec les virgules. | `true` | |
| `grammalecte.runOnTheFly` | Lancer Grammalecte au fur et à mesure quand vous tapez le texte ou sauvegardez le document. | `true` | Si vous trouvez que voir les erreurs au fur et à mesure est un problème et que vous préférez voir et traiter les erreurs quand vous voulez, désactivez cette option et utilisez les commandes `grammalecte.check` et `grammalecte.clear`. |

### Licence

* Cette extension n'est pas liée à Grammalecte.
* Cette extension est publiée sous la licence MIT dont le texte est inclus dans ce dépôt.
* Grammalecte est publiée sous la license GPLv3.
  * Cette différence de license n'est pas un problème : cette extension fait des apples basiques à la CLI de Grammalecte. Ce cas est couvert par la GPL, voir [ici](https://www.gnu.org/licenses/gpl-faq.html#GPLPlugins) pour en savoir plus.
